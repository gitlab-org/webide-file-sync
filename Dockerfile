###### Builder stage ######
FROM golang:1.11 as builder

WORKDIR /root/webide-file-sync
COPY . .

ENV GO111MODULE=on
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 make

###### Runtime stage #######
FROM alpine:3.9

# Packages needed to run the app
RUN apk update && apk upgrade \
  && apk add ca-certificates \
  && apk add git

WORKDIR /root/
COPY --from=builder /root/webide-file-sync/webide-file-sync .

ENTRYPOINT ["./webide-file-sync"]
