EXE_ALL := webide-file-sync

.PHONY:	all
all:	clean $(EXE_ALL)

$(EXE_ALL):
	go build -o $@ gitlab.com/gitlab-org/webide-file-sync/cmd/webide-file-sync

.PHONY:	test
test: prepare-tests
	go test -v ./... -p 1

.PHONY:	clean
clean:	clean-webide-file-sync clean-build
	$(call message,$@)
	rm -rf testdata/data

.PHONY:	clean-webide-file-sync
clean-webide-file-sync:
	$(call message,$@)
	rm -f $(EXE_ALL)

.PHONY:	clean-build
clean-build:
	$(call message,$@)
	rm -rf $(TARGET_DIR)

.PHONY:	prepare-tests
prepare-tests:	testdata/data/group/test.git $(EXE_ALL)

testdata/data/group/test.git:
	$(call message,$@)
	git clone --quiet https://gitlab.com/gitlab-org/gitlab-test.git $@
