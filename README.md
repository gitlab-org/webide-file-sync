# WebIDE File Sync

The Web IDE File Sync service allows performing operations over a git repository. The
service only supports (and enforce) one client at a time.

## Communication Layers
The service uses different communication layers:
- Websocket Layer
- Transport Layer
- Application Layer

### Websocket Layer
For this layer, the library [Gorilla Websocket](https://github.com/gorilla/websocket) is used. In
this level the communication is done according to the [Websocket RFC](https://tools.ietf.org/html/rfc6455).

In the client side, we can use the standard the [Websocket API](https://developer.mozilla.org/es/docs/Web/API/WebSocket). Any event
and/or error coming from the server will be handled using the standard callbacks. Once inside those
callbacks, the messages will have to be decoded inside them.

For example, any `Close` event, will have the standard frame structure.

### Transport Layer
In this layer, the service implement a structure for communication, named as `Message`:

```go
type Message struct {
	Code          MessageCode     `json:"code"`
	CorrelationID int             `json:"correlation_id"`
	Namespace     string          `json:"namespace"`
	EventName     string          `json:"event"`
	Error         ErrorMessage    `json:"error"`
	Payload       json.RawMessage `json:"payload"`
}
```

- Field `Code` identifies which type of message it is. The possible values are:
```go
const (
	Connect    MessageCode = "CONNECT"
	Disconnect MessageCode = "DISCONNECT"
	Event      MessageCode = "EVENT"
	Ack        MessageCode = "ACK"
	Ping       MessageCode = "PING"
	Pong       MessageCode = "PONG"
)
```

Any response to each transport request will have the code `ACK`. The code `PING` identifies
a special type of message used to check that the server connection is alive.

- Field `CorrelationID` is used to identify which request is the response from. It can happen that,
when a request takes some time and another one (faster one) comes in, this second response
is returned before the first one. That's why we need this field to identify the request and
the response

- Field `Error` will store any error triggered that comes from the communication, like decoding or encoding
messages. THe structure of this field is:
```go
type ErrorMessage struct {
	Code    int    `json:"code"`
	Message string `json:"message`
}
```

- Field `Payload` stores and encapsulates the information coming to/from the application layer.

### Application Layer

The service exposes two main endpoints: `/files` and `/repository`.

Through the `files` endpoint, the following operations can be performed:
- `PATCH`: applies a multi-file unified git diff to the repository and also can delete files:
```javascript
<script>
  var socket = new WebSocket("ws://localhost:3000");
  var diff = "diff --git a/newfile b/newfile
              new file mode 100644
              index 0000000..a23655c
              --- /dev/null
              +++ b/newfile
              @@ -0,0 +1 @@
              +foo"
  var msg = {
          code: "EVENT",
          namespace: "/files",
          event: "PATCH",
          payload: {"diff": diff, "delete_files": ["CHANGELOG", "README.md"]}
        };

  socket.send(JSON.stringify(msg))
</script>
```

This endpoint expects the whole differential diff from `HEAD`. If the operation fails it
resets the repository and cleans any untracked file.

The `repository` endpoint exposes the following actions:
- `DELETE`: resets the working tree of the repository and cleans any
untracked file.
```javascript
<script>
  var socket = new WebSocket("ws://localhost:3000");
  var msg = {
          code: "EVENT",
          namespace: "/repository",
          event: "DELETE",
        };

  socket.send(JSON.stringify(msg))
</script>
```

The structure of the response to any of these endpoints is:
```go
type Response struct {
	StatusCode   int    `json:"status_code"`
	ErrorMessage string `json:"error_message"`
}
```

The result of the operation will be stored in this structure, including any error.

Summing up, errors can be stored:
- In the websocket frame. They'll have to do mostly with communication stuff
- In the `Message` structure if the error comes as a result of a failure in the transport layer
- In the `Response` structure (inside `Message`), when the error was triggered in the application layer.

## Run
In order to start the service you have to run the command with a valid
git repository path in `project-dir`.

```bash
make
webide-file-sync -project-dir /path/to/the/git/repository
```

The executable also accepts a list of valid origins to accept requests from.
```bash
make
webide-file-sync -project-dir /path/to/the/git/repository -valid-origins gitlab.com
```

By default, the service starts in the port `3000` but you can provide
a port:
```bash
make
webide-file-sync -port 5000 -project-dir /path/to/the/git/repository
```

In the [example](./example/static/index.html) there is an example showing how
to use all the messages and some of the basic workflows.

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
