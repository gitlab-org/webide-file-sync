package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/webide-file-sync/eventmanager"
)

const (
	defaultPort = 3000
	logFileName = "events.log"
)

var (
	projectDirFlag   = "project-dir"
	validOriginsFlag = "valid-origins"
)

func main() {
	var port int
	var projectPath string
	var validOrigins string

	flag.IntVar(&port, "port", defaultPort, "Server Port")
	flag.StringVar(&projectPath, projectDirFlag, "", "Repository project path")
	flag.StringVar(&validOrigins, validOriginsFlag, "", "List of valid and trusted origins separated by a comma (i.e gitlab.com,foo.com)")
	flag.Parse()

	checkRequiredFlag(projectDirFlag, projectPath)

	var origins []string
	if validOrigins != "" {
		origins = strings.Split(validOrigins, ",")
	}

	initLogger()
	em, err := eventmanager.New(projectPath, port, origins)
	if err != nil {
		logrus.Fatalf("Failed to create event manager: %+v", err)
	}

	if err := em.Start(); err != nil {
		logrus.Fatalf("Failed to start event manager: %+v", err)
	}
}

func initLogger() {
	file, err := os.OpenFile(logFilePath(), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err == nil {
		logrus.SetOutput(file)
		return
	}
	logrus.Info("Failed to log to file, using default stderr")
}

func logFilePath() string {
	return filepath.Join(os.TempDir(), logFileName)
}

func checkRequiredFlag(stringFlag string, value string) {
	if value != "" {
		return
	}

	fmt.Fprintf(flag.CommandLine.Output(), "Flag %s is required\n", stringFlag)
	flag.Usage()
	os.Exit(1)
}
