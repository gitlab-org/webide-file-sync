package common

import (
	"net"
	"strings"
)

func NormalizedHostname(hostport string) string {
	host, _, err := net.SplitHostPort(hostport)
	if err != nil {
		host = hostport
	}

	return strings.ToLower(host)
}
