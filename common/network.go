package common

import "encoding/json"

type Response struct {
	StatusCode   int    `json:"status_code"`
	ErrorMessage string `json:"error_message"`
}

type PatchRequest struct {
	Diff        string   `json:"diff"`
	DeleteFiles []string `json:"delete_files"`
}

type MessageCode string

const (
	Connect    MessageCode = "CONNECT"
	Disconnect MessageCode = "DISCONNECT"
	Event      MessageCode = "EVENT"
	Ack        MessageCode = "ACK"
	Ping       MessageCode = "PING"
	Pong       MessageCode = "PONG"
)

type ErrorMessage struct {
	Code    int    `json:"code"`
	Message string `json:"message`
}

type Message struct {
	Code          MessageCode     `json:"code"`
	CorrelationID int             `json:"correlation_id"`
	Namespace     string          `json:"namespace"`
	EventName     string          `json:"event"`
	Error         ErrorMessage    `json:"error"`
	Payload       json.RawMessage `json:"payload"`
}
