package eventmanager

import (
	"gitlab.com/gitlab-org/webide-file-sync/filemanager"
)

func (em *EventManager) execPatch(diff string, deleteFiles []string) *filemanager.Error {
	// if the diff is empty and there are no files to delete,
	// there is no need to reset the repository
	if diff == "" && len(deleteFiles) == 0 {
		return nil
	}

	// Reset changes in the index
	if err := em.filemanager.ResetChanges(); err != nil {
		// Return the error and let the caller decide what to do
		return err
	}

	// Apply the diff
	if err := em.filemanager.Patch(diff); err != nil {
		// Return the error and let the caller decide what to do
		return err
	}

	if err := em.filemanager.Delete(deleteFiles); err != nil {
		// Clean the changes in the index made by Patch
		em.filemanager.ResetChanges()

		// Return the error and let the caller decide what to do
		return err
	}

	// Update the index and the work tree
	if err := em.filemanager.CheckoutFiles(); err != nil {
		return err
	}

	return nil
}

func (em *EventManager) execResetRepository() *filemanager.Error {
	return em.filemanager.ResetRepository()
}
