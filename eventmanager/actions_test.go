package eventmanager

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/webide-file-sync/testhelper"
)

func TestExecPatchReturnsWhenNoData(t *testing.T) {
	em := newEventManager(t)
	defer func() {
		require.Nil(t, em.execResetRepository())
	}()

	file := "README"
	expected := []byte("foobar")
	testhelper.WriteFileInRepo(t, file, expected)

	customErr := em.execPatch("", []string{})
	require.Nil(t, customErr)

	content := testhelper.ReadFileInRepo(t, file)
	require.Equal(t, expected, content)
}

func TestExecPatchRepositoryResetWhenDiff(t *testing.T) {
	em := newEventManager(t)
	defer func() {
		require.Nil(t, em.execResetRepository())
	}()

	file := "README"
	expected := []byte("foobar")
	originalContent := testhelper.ReadFileInRepo(t, file)

	testhelper.WriteFileInRepo(t, file, expected)

	diff := testhelper.ReadFileInData(t, "newfile.diff")
	customErr := em.execPatch(string(diff), []string{})
	require.Nil(t, customErr)

	content := testhelper.ReadFileInRepo(t, file)
	require.NotEqual(t, expected, content)
	require.Equal(t, originalContent, content)
}

func TestExecPatchRepositoryResetWhenDeleteFiles(t *testing.T) {
	em := newEventManager(t)
	defer func() {
		require.Nil(t, em.execResetRepository())
	}()

	file := "README"
	expected := []byte("foobar")
	testhelper.WriteFileInRepo(t, file, expected)

	customErr := em.execPatch("", []string{"CHANGELOG"})
	require.Nil(t, customErr)

	content := testhelper.ReadFileInRepo(t, file)
	require.NotEqual(t, expected, content)
}

func TestExecPatchSuccessful(t *testing.T) {
	em := newEventManager(t)
	defer func() {
		require.Nil(t, em.execResetRepository())
	}()

	nonTrackedFile := createFile(t, "new_file")
	require.True(t, existsFile(nonTrackedFile))

	diff := testhelper.ReadFileInData(t, "newfile.diff")
	customErr := em.execPatch(string(diff), []string{"CHANGELOG"})
	require.Nil(t, customErr)

	require.True(t, existsFile(testhelper.FileInRepo("non-existent-file123")))
	require.False(t, existsFile(testhelper.FileInRepo("CHANGELOG")))
	require.True(t, existsFile(nonTrackedFile))
}

func newEventManager(t *testing.T) *EventManager {
	em, err := New(testhelper.TestRepoPath(), 25476, nil)
	require.NoError(t, err)

	return em
}

func createFile(t *testing.T, filename string) string {
	pathFile := testhelper.FileInRepo(filename)
	err := ioutil.WriteFile(pathFile, []byte("foobar"), 0644)
	require.NoError(t, err)

	return pathFile
}

func existsFile(path string) bool {
	_, err := os.Stat(path)

	return err == nil
}
