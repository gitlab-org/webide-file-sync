package eventmanager

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/webide-file-sync/common"
	"gitlab.com/gitlab-org/webide-file-sync/filemanager"
	"gitlab.com/gitlab-org/webide-file-sync/server"
	"gitlab.com/gitlab-org/webide-file-sync/websocketserver"
)

// EventManager holds two objects: the Websocket server and the FileManager.
// This object is the coordinator among the incoming events, the git
// actions and the outcoming responses.
type EventManager struct {
	server      *server.Server
	filemanager *filemanager.FileManager
}

// New instantiates a new EventManager
func New(path string, port int, origins []string) (*EventManager, error) {
	fm, err := filemanager.New(path)
	if err != nil {
		return nil, err
	}

	server, serverErr := server.New(port, origins)
	if serverErr != nil {
		return nil, serverErr
	}

	em := &EventManager{server: server, filemanager: fm}
	em.configureEvents()

	return em, nil
}

// Start starts the Websocket Server
func (em *EventManager) Start() error {
	return em.server.Serve()
}

// Stop stops the Websocket Server
func (em *EventManager) Stop() error {
	return em.server.Close()
}

func (em *EventManager) configureEvents() {
	em.eventServer().OnConnect("/files", func(conn *websocket.Conn) *common.Response {
		em.logEvent("/files", "CONNECT", logrus.Fields{})
		em.filemanager.Start()

		return successResponse()
	})

	em.eventServer().OnDisconnect("/files", func(conn *websocket.Conn) *common.Response {
		em.logEvent("/files", "DISCONNECT", logrus.Fields{})
		em.filemanager.Stop()

		return successResponse()
	})

	em.eventServer().OnEvent("/files", "PATCH", func(conn *websocket.Conn, payload json.RawMessage) *common.Response {
		var request *common.PatchRequest
		if err := json.Unmarshal(payload, &request); err != nil {
			return jsonErrorResponse(err)
		}

		em.logEvent("/files", "PATCH", logrus.Fields{})
		err := em.execPatch(request.Diff, request.DeleteFiles)
		if err != nil {
			em.logError("/files", "PATCH", err)
			return errorResponse(err)
		}
		return successResponse()
	})

	fn := func(conn *websocket.Conn) *common.Response {
		em.logEvent("/repository", "DELETE", logrus.Fields{})
		err := em.execResetRepository()
		if err != nil {
		}

		return successResponse()
	}

	em.eventServer().OnEvent(
		"/repository",
		"DELETE",
		websocketserver.NoPayload(fn),
	)
}

func (em *EventManager) eventServer() *websocketserver.Server {
	return em.server.EventServer()
}

func (em *EventManager) logEvent(namespace string, event string, others map[string]interface{}) {
	logrus.WithFields(logrus.Fields{
		"namespace": namespace,
		"event":     event,
	}).WithFields(others).Info("Received Event")
}

func (em *EventManager) logError(namespace string, event string, err error) {
	logrus.WithFields(logrus.Fields{
		"namespace": namespace,
		"event":     event,
	}).Error(err.Error())
}

func jsonErrorResponse(err error) *common.Response {
	return &common.Response{StatusCode: http.StatusBadRequest, ErrorMessage: "Decoding json failed:" + err.Error()}
}

func errorResponse(err *filemanager.Error) *common.Response {
	return &common.Response{StatusCode: err.Code(), ErrorMessage: err.Error()}
}

func successResponse() *common.Response {
	return &common.Response{StatusCode: http.StatusOK}
}
