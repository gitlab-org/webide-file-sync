package eventmanager

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/webide-file-sync/common"
	"gitlab.com/gitlab-org/webide-file-sync/testhelper"
)

func TestFilesPatchEndpointSuccessful(t *testing.T) {
	diff, err := ioutil.ReadFile(testhelper.FileInData("newfile.diff"))
	require.NoError(t, err)

	var resp common.Response
	performRequest(t, "/files", "PATCH", common.PatchRequest{Diff: string(diff), DeleteFiles: []string{"CHANGELOG"}}, &resp)

	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestFilesPatchEndpointWithErrorResponse(t *testing.T) {
	diff, err := ioutil.ReadFile(testhelper.FileInData("invalidupdate.diff"))
	require.NoError(t, err)

	var resp common.Response
	performRequest(t, "/files", "PATCH", common.PatchRequest{Diff: string(diff)}, &resp)

	require.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode)
	require.Equal(t, "error applying the patch", resp.ErrorMessage)

}

func TestRepositoryDeleteEndpointSuccessful(t *testing.T) {
	var resp common.Response
	performRequest(t, "/repository", "DELETE", nil, &resp)

	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestRepositoryDeleteEndpointError(t *testing.T) {
	var resp common.Response
	performRequest(t, "/repository", "DELETE", "whatever", &resp)

	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
	require.Equal(t, "protocol error", resp.ErrorMessage)
}

func performRequest(t *testing.T, namespace string, event string, req interface{}, response interface{}) {
	port := 25476
	em, err := New(testhelper.TestRepoPath(), port, nil)
	require.NoError(t, err)

	go em.Start()
	defer func() {
		require.NoError(t, em.Stop())
		require.Nil(t, em.execResetRepository())
	}()

	u := fmt.Sprintf("ws://localhost:%d", port)

	result := sendRequest(t, u, namespace, event, req)
	err = json.Unmarshal(result.Payload, response)
	require.NoError(t, err)
}

func sendRequest(t *testing.T, url string, namespace string, event string, request interface{}) common.Message {
	msg := common.Message{Code: common.Event, Namespace: namespace, EventName: event}
	encoded, err := json.Marshal(request)
	require.NoError(t, err)
	msg.Payload = encoded

	ws, _, err := websocket.DefaultDialer.Dial(url, nil)
	require.NoError(t, err)

	err = ws.WriteJSON(msg)
	require.NoError(t, err)

	var response common.Message
	err = ws.ReadJSON(&response)
	if err != nil {
		require.NoError(t, err)
	}
	return response
}
