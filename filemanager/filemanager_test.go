package filemanager

import (
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/webide-file-sync/testhelper"
)

func TestPatchErrorInvalidDiff(t *testing.T) {
	fm := newFileManager(t)
	customErr := fm.Patch("non-valid-diff")

	require.NotNil(t, customErr)
	require.Equal(t, http.StatusUnprocessableEntity, customErr.Code())
	require.Equal(t, "error applying the patch", customErr.Error())
}

func TestPatchEmptyDiff(t *testing.T) {
	fm := newFileManager(t)
	customErr := fm.Patch("")

	require.Nil(t, customErr)
}

func TestPatchError(t *testing.T) {
	fm := newFileManager(t)
	defer fm.ResetRepository()

	diff := testhelper.ReadFileInData(t, "partial.diff")
	customErr := fm.Patch(string(diff))
	require.NotNil(t, customErr)
	require.Equal(t, http.StatusUnprocessableEntity, customErr.Code())
	require.Equal(t, "error applying the patch", customErr.Error())
}

func TestPatchSuccess(t *testing.T) {
	fm := newFileManager(t)
	defer fm.ResetRepository()

	diff := testhelper.ReadFileInData(t, "newfile.diff")
	customErr := fm.Patch(string(diff))
	require.Nil(t, customErr)
}

func TestDeleteSuccessful(t *testing.T) {
	fm := newFileManager(t)
	defer fm.ResetRepository()

	fileNames := []string{"README.md", "CHANGELOG", ""}
	customErr := fm.Delete(fileNames)
	require.Nil(t, customErr)

	for index := 0; index < len(fileNames); index++ {
		if fileNames[index] == "" {
			continue
		}

		pathFile := testhelper.FileInRepo(fileNames[index])
		_, err := os.Stat(pathFile)
		require.Error(t, err)
		require.Equal(t, true, os.IsNotExist(err))
	}
}

func TestDeleteError(t *testing.T) {
	fm := newFileManager(t)
	defer fm.ResetRepository()

	fileName := "non-existent-file"
	customErr := fm.Delete([]string{fileName})
	require.Equal(t, http.StatusUnprocessableEntity, customErr.Code())
}

func TestResetRepository(t *testing.T) {
	fm := newFileManager(t)
	defer fm.ResetRepository()

	fileName := "README.md"
	customErr := fm.Delete([]string{fileName})
	require.Nil(t, customErr)

	pathFile := testhelper.FileInRepo(fileName)
	_, err := os.Stat(pathFile)
	require.Equal(t, true, os.IsNotExist(err))

	customErr = fm.ResetRepository()
	require.Nil(t, customErr)

	_, err = os.Stat(pathFile)
	require.NoError(t, err)
}

func newFileManager(t *testing.T) *FileManager {
	fm, customErr := New(testhelper.TestRepoPath())
	require.Nil(t, customErr)

	return fm
}
