package filemanager

import (
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/webide-file-sync/testhelper"
)

func TestIsGitRepositorySuccessful(t *testing.T) {
	fm := newFileManager(t)
	customErr := fm.isGitRepository()

	require.Nil(t, customErr)
}

func TestIsGitRepositoryFailsNotExist(t *testing.T) {
	fm := newFileManager(t)
	fm.Path = "non_existent_path"
	customErr := fm.isGitRepository()

	require.NotNil(t, customErr)
	require.Equal(t, http.StatusNotFound, customErr.Code())
}

func TestIsGitRepositoryFailsFile(t *testing.T) {
	fm := newFileManager(t)

	file, err := ioutil.TempFile("", "bar")
	require.NoError(t, err)
	defer os.Remove(file.Name())

	fm.Path = file.Name()
	customErr := fm.isGitRepository()
	require.Equal(t, http.StatusUnprocessableEntity, customErr.Code())
	require.Equal(t, "the path must be a directory", customErr.Error())
}

func TestIsGitRepositoryFailsNormalDir(t *testing.T) {
	fm := newFileManager(t)

	dir, err := ioutil.TempDir("", "bar")
	require.NoError(t, err)
	defer os.RemoveAll(dir)

	fm.Path = dir
	customErr := fm.isGitRepository()
	require.Equal(t, http.StatusUnprocessableEntity, customErr.Code())
	require.Equal(t, "the path is not a git repository", customErr.Error())
}

func TestRunGitFilesCheckout(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	testFile := "README"
	originalContent := testhelper.ReadFileInRepo(t, testFile)

	testhelper.WriteFileInRepo(t, testFile, []byte("foobar"))

	updatedContent := testhelper.ReadFileInRepo(t, testFile)
	require.NotEqual(t, originalContent, updatedContent)

	require.Nil(t, fm.runGitFilesCheckout())

	checkoutContent := testhelper.ReadFileInRepo(t, testFile)
	require.Equal(t, originalContent, checkoutContent)
}

func TestRunGitApplyCheck(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	diff := testhelper.ReadFileInData(t, "newfile.diff")

	require.Nil(t, fm.runGitApply(string(diff), true))

	pathFile := testhelper.FileInRepo("non-existent-file123")
	_, err := os.Stat(pathFile)
	require.Equal(t, true, os.IsNotExist(err))
}

func TestRunGitApply(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	diff := testhelper.ReadFileInData(t, "newfile.diff")

	require.Nil(t, fm.runGitApply(string(diff), false))
	require.Nil(t, fm.runGitFilesCheckout())

	pathFile := testhelper.FileInRepo("non-existent-file123")
	_, err := os.Stat(pathFile)
	require.NoError(t, err)
}

func TestRunGitDelete(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	file := "README"
	pathFile := testhelper.FileInRepo(file)
	testhelper.WriteFileInRepo(t, file, []byte("foobar"))

	require.Nil(t, fm.runGitDelete([]string{file}))

	_, err := os.Stat(pathFile)
	require.Equal(t, true, os.IsNotExist(err))
}

func TestRunGitDeleteDirectories(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	pathFile := testhelper.FileInRepo("bar")
	_, err := os.Stat(pathFile)
	require.NoError(t, err)
	require.Nil(t, fm.runGitDelete([]string{pathFile}))

	_, err = os.Stat(pathFile)
	require.Equal(t, true, os.IsNotExist(err))
}

func TestRunGitDeleteForce(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	file := "README"
	pathFile := testhelper.FileInRepo(file)
	testhelper.WriteFileInRepo(t, file, []byte("foobar"))

	require.Nil(t, fm.runGitDelete([]string{pathFile}))

	_, err := os.Stat(pathFile)
	require.Equal(t, true, os.IsNotExist(err))
}

func TestRunResetHard(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	pathFile := testhelper.FileInRepo("README")
	err := os.Remove(pathFile)
	require.NoError(t, err)

	_, err = os.Stat(pathFile)
	require.Equal(t, true, os.IsNotExist(err))

	fm.runGitResetHard()

	_, err = os.Stat(pathFile)
	require.NoError(t, err)
}

func TestRunCleanRepository(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	file := "non-existent-file123"
	pathFile := testhelper.FileInRepo(file)
	testhelper.WriteFileInRepo(t, file, []byte("foobar"))

	require.Nil(t, fm.runCleanRepository())

	_, err := os.Stat(pathFile)
	require.Equal(t, true, os.IsNotExist(err))
}

func TestRunGitResetHard(t *testing.T) {
	fm := newFileManager(t)
	require.Nil(t, fm.runGitResetHard())

	file := "foobar-file"
	newFile := testhelper.FileInRepo(file)
	testhelper.WriteFileInRepo(t, file, []byte("foobar"))

	diff := testhelper.ReadFileInData(t, "newfile.diff")
	require.Nil(t, fm.runGitApply(string(diff), false))
	require.Nil(t, fm.runGitFilesCheckout())

	pathFile := testhelper.FileInRepo("non-existent-file123")
	_, err := os.Stat(pathFile)
	require.NoError(t, err)

	require.Nil(t, fm.runGitResetHard())

	_, err = os.Stat(pathFile)
	require.Equal(t, true, os.IsNotExist(err))

	_, err = os.Stat(newFile)
	require.NoError(t, err)
}

func TestBuildGitCommand(t *testing.T) {
	fm := newFileManager(t)

	cmd, customErr := fm.buildGitCommand("whatever")
	require.Nil(t, customErr)

	gitPath, err := gitPath()
	require.NoError(t, err)
	require.Equal(t, cmd.Dir, fm.Path)
	require.Equal(t, cmd.Args, []string{gitPath, "whatever"})
}
