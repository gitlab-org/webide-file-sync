module gitlab.com/gitlab-org/webide-file-sync

replace github.com/wedeploy/gosocketio => gitlab.com/fjsanpedro/gosocketio v0.0.0-20190402175147-4e09e6828624

require (
	github.com/gorilla/mux v1.7.1
	github.com/gorilla/websocket v1.4.0
	github.com/sirupsen/logrus v1.4.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190211182817-74369b46fc67 // indirect
	golang.org/x/sys v0.0.0-20181029174526-d69651ed3497 // indirect
)
