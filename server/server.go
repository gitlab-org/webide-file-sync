package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/gitlab-org/webide-file-sync/websocketserver"
)

type Server struct {
	port            int
	websocketServer *websocketserver.Server
	httpServer      *http.Server
}

// New returns a new Server
func New(port int, origins []string) (*Server, error) {
	server, err := websocketserver.NewServer(origins, websocketserver.DefaultPongWait)
	if err != nil {
		return nil, err
	}

	return &Server{port, server, nil}, nil
}

// Serve starts the websocket server
func (s *Server) Serve() error {
	// start processing the message queue
	router := mux.NewRouter()
	router.HandleFunc("/", s.websocketServer.ServeHTTP)

	s.httpServer = &http.Server{Addr: fmt.Sprintf(":%d", s.port), Handler: router}
	log.Println("Starting server in port", s.port, "...")
	return s.httpServer.ListenAndServe()
}

// Close stops the http server and the websocket server
func (s *Server) Close() error {
	defer s.websocketServer.Close()

	return s.httpServer.Close()
}

// EventServer returns the current websocket server
func (s *Server) EventServer() *websocketserver.Server {
	return s.websocketServer
}
