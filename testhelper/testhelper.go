package testhelper

import (
	"errors"
	"io/ioutil"
	"path"
	"runtime"
	"testing"

	"github.com/stretchr/testify/require"
)

const testRepoRoot = "testdata/data"
const testRepo = "group/test.git"
const binaryDir = "webide-file-sync"

func RootDir() string {
	_, currentFile, _, ok := runtime.Caller(0)
	if !ok {
		panic(errors.New("error getting root directory"))
	}
	return path.Join(path.Dir(currentFile), "../..")
}

func TestRepoPath() string {
	return path.Join(RootDir(), binaryDir, testRepoRoot, testRepo)
}

func TestDataDir() string {
	return path.Join(RootDir(), binaryDir, "testdata")
}

func FileInRepo(filename string) string {
	return path.Join(TestRepoPath(), filename)
}

func FileInData(filename string) string {
	return path.Join(TestDataDir(), filename)
}

func ReadFileInRepo(t *testing.T, filename string) []byte {
	return ReadFile(t, FileInRepo(filename))
}

func WriteFileInRepo(t *testing.T, filename string, content []byte) {
	file := FileInRepo(filename)
	err := ioutil.WriteFile(file, content, 0644)
	require.NoError(t, err)
}

func ReadFileInData(t *testing.T, filename string) []byte {
	return ReadFile(t, FileInData(filename))
}

func ReadFile(t *testing.T, path string) []byte {
	content, err := ioutil.ReadFile(path)
	require.NoError(t, err)

	return content
}
