package websocketserver

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/webide-file-sync/common"
)

func TestMessageDecodingError(t *testing.T) {
	wsServer, httpServer := newServers(t, 0, nil)
	defer httpServer.Close()
	defer wsServer.Close()

	ws, _, err := websocket.DefaultDialer.Dial(testServerURL(httpServer), validHeaders())
	require.NoError(t, err)

	err = ws.WriteMessage(websocket.TextMessage, []byte("foobar"))
	require.NoError(t, err)

	var response common.Message
	err = ws.ReadJSON(&response)
	require.Error(t, err)
	require.Equal(t, true, websocket.IsUnexpectedCloseError(err))
}

func TestMessageFormatError(t *testing.T) {
	testCases := []struct {
		desc         string
		code         common.MessageCode
		namespace    string
		errorMessage string
	}{
		{
			desc:         "empty message code",
			namespace:    "foobar",
			errorMessage: "invalid message",
		},
		{
			desc:         "invalid Message Code",
			code:         "invalidCode",
			namespace:    "foobar",
			errorMessage: "invalid message",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.desc, func(t *testing.T) {
			wsServer, httpServer := newServers(t, 0, nil)
			wsServer.OnConnect(testCase.namespace, func(*websocket.Conn) *common.Response { return &common.Response{} })

			msg := common.Message{Code: testCase.code, CorrelationID: 1, Namespace: testCase.namespace}
			_, response := sendMessage(t, testServerURL(httpServer), msg, validHeaders())

			require.Equal(t, common.Ack, response.Code)
			require.Equal(t, 1, response.CorrelationID)
			require.Equal(t, 500, response.Error.Code)
			require.Equal(t, testCase.errorMessage, response.Error.Message)

			httpServer.Close()
			wsServer.Close()
		})
	}
}

func TestOnConnect(t *testing.T) {
	testCases := []struct {
		desc         string
		namespace    string
		errorMessage string
	}{
		{
			desc:         "empty namespace",
			errorMessage: "namespace  is not registered",
		},
		{
			desc:         "with non registered namespace",
			errorMessage: "namespace  is not registered",
		},
		{
			desc:      "calls callback function",
			namespace: "foobar",
		},
	}

	returnedValue := common.Response{StatusCode: http.StatusBadGateway, ErrorMessage: "foobar"}
	fn := func(*websocket.Conn) *common.Response { return &returnedValue }

	for _, testCase := range testCases {
		t.Run(testCase.desc, func(t *testing.T) {
			wsServer, httpServer := newServers(t, 0, nil)
			wsServer.OnConnect(testCase.namespace, fn)

			msg := common.Message{Code: common.Connect, Namespace: testCase.namespace, CorrelationID: 1}
			_, response := sendMessage(t, testServerURL(httpServer), msg, validHeaders())

			require.Equal(t, common.Ack, response.Code)
			require.Equal(t, 1, response.CorrelationID)

			if testCase.errorMessage != "" {
				require.Equal(t, 500, response.Error.Code)
				require.Equal(t, testCase.errorMessage, response.Error.Message)
			} else {
				var encoded common.Response
				err := json.Unmarshal(response.Payload, &encoded)
				require.NoError(t, err)
				require.Equal(t, returnedValue, encoded)
			}

			httpServer.Close()
			wsServer.Close()
		})
	}
}

func TestOnConnectOverwrite(t *testing.T) {
	wsServer, httpServer := newServers(t, 0, nil)
	defer httpServer.Close()
	defer wsServer.Close()

	namespace := "foo"
	firstResponse := common.Response{StatusCode: http.StatusBadGateway, ErrorMessage: "foobar"}
	expectedResponse := common.Response{StatusCode: http.StatusAccepted, ErrorMessage: "foobar2"}
	wsServer.OnConnect(namespace, func(*websocket.Conn) *common.Response { return &firstResponse })
	wsServer.OnConnect(namespace, func(*websocket.Conn) *common.Response { return &expectedResponse })

	msg := common.Message{Code: common.Connect, Namespace: namespace, CorrelationID: 1}
	_, response := sendMessage(t, testServerURL(httpServer), msg, validHeaders())

	require.Equal(t, common.Ack, response.Code)
	require.Equal(t, 1, response.CorrelationID)

	var encoded common.Response
	err := json.Unmarshal(response.Payload, &encoded)
	require.NoError(t, err)
	require.Equal(t, expectedResponse, encoded)
}

func TestOnDisconnect(t *testing.T) {
	testCases := []struct {
		desc         string
		namespace    string
		errorMessage string
	}{
		{
			desc:         "empty namespace",
			errorMessage: "namespace  is not registered",
		},
		{
			desc:         "with non registered namespace",
			errorMessage: "namespace  is not registered",
		},
		{
			desc:      "calls callback function",
			namespace: "foobar",
		},
	}

	returnedValue := common.Response{StatusCode: http.StatusBadGateway, ErrorMessage: "foobar"}
	fn := func(*websocket.Conn) *common.Response { return &returnedValue }
	for _, testCase := range testCases {
		t.Run(testCase.desc, func(t *testing.T) {
			wsServer, httpServer := newServers(t, 0, nil)
			wsServer.OnDisconnect(testCase.namespace, fn)

			msg := common.Message{Code: common.Disconnect, Namespace: testCase.namespace, CorrelationID: 1}
			_, response := sendMessage(t, testServerURL(httpServer), msg, validHeaders())

			require.Equal(t, common.Ack, response.Code)
			require.Equal(t, 1, response.CorrelationID)

			if testCase.errorMessage != "" {
				require.Equal(t, 500, response.Error.Code)
				require.Equal(t, testCase.errorMessage, response.Error.Message)
			} else {
				var encoded common.Response
				err := json.Unmarshal(response.Payload, &encoded)
				require.NoError(t, err)
				require.Equal(t, returnedValue, encoded)
			}

			httpServer.Close()
			wsServer.Close()
		})
	}
}

func TestOnDisConnectOverwrite(t *testing.T) {
	wsServer, httpServer := newServers(t, 0, nil)
	defer httpServer.Close()
	defer wsServer.Close()

	namespace := "foo"
	firstResponse := common.Response{StatusCode: http.StatusBadGateway, ErrorMessage: "foobar"}
	expectedResponse := common.Response{StatusCode: http.StatusAccepted, ErrorMessage: "foobar2"}
	wsServer.OnDisconnect(namespace, func(*websocket.Conn) *common.Response { return &firstResponse })
	wsServer.OnDisconnect(namespace, func(*websocket.Conn) *common.Response { return &expectedResponse })

	msg := common.Message{Code: common.Disconnect, Namespace: namespace, CorrelationID: 1}
	_, response := sendMessage(t, testServerURL(httpServer), msg, validHeaders())

	require.Equal(t, common.Ack, response.Code)
	require.Equal(t, 1, response.CorrelationID)

	var encoded common.Response
	err := json.Unmarshal(response.Payload, &encoded)
	require.NoError(t, err)
	require.Equal(t, expectedResponse, encoded)
}

func TestOnEvent(t *testing.T) {
	event := "event"

	expectedResponse := common.Response{StatusCode: http.StatusBadGateway, ErrorMessage: "foobar"}
	fn := func(*websocket.Conn, json.RawMessage) *common.Response { return &expectedResponse }

	testCases := []struct {
		desc         string
		namespace    string
		event        string
		errorMessage string
	}{
		{
			desc:         "empty namespace",
			errorMessage: "namespace  is not registered",
		},
		{
			desc:         "with non registered namespace",
			errorMessage: "namespace  is not registered",
		},
		{
			desc:         "empty event",
			namespace:    "foobar",
			errorMessage: "event cannot be empty",
		},
		{
			desc:         "not registered event",
			namespace:    "foobar",
			event:        "foo",
			errorMessage: "no callback associated with event foo",
		},
		{
			desc:      "calls callback function",
			namespace: "foobar",
			event:     event,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.desc, func(t *testing.T) {
			wsServer, httpServer := newServers(t, 0, nil)
			wsServer.OnEvent(testCase.namespace, event, fn)

			payload := common.PatchRequest{Diff: "foobar"}
			encodedPayload, err := json.Marshal(payload)
			require.NoError(t, err)

			msg := common.Message{Code: common.Event, Namespace: testCase.namespace, EventName: testCase.event, CorrelationID: 1, Payload: encodedPayload}
			_, response := sendMessage(t, testServerURL(httpServer), msg, validHeaders())

			require.Equal(t, common.Ack, response.Code)
			require.Equal(t, 1, response.CorrelationID)

			if testCase.errorMessage != "" {
				require.Equal(t, 500, response.Error.Code)
				require.Equal(t, testCase.errorMessage, response.Error.Message)
			} else {
				var encoded common.Response
				err := json.Unmarshal(response.Payload, &encoded)
				require.NoError(t, err)
				require.Equal(t, expectedResponse, encoded)
			}

			httpServer.Close()
			wsServer.Close()
		})
	}
}

func TestOnEventOverwrite(t *testing.T) {
	wsServer, httpServer := newServers(t, 0, nil)
	defer httpServer.Close()
	defer wsServer.Close()

	namespace := "foo"
	event := "event"
	firstResponse := common.Response{StatusCode: http.StatusBadGateway, ErrorMessage: "foobar"}
	expectedResponse := common.Response{StatusCode: http.StatusAccepted, ErrorMessage: "foobar2"}
	wsServer.OnEvent(namespace, event, func(*websocket.Conn, json.RawMessage) *common.Response { return &firstResponse })
	wsServer.OnEvent(namespace, event, func(*websocket.Conn, json.RawMessage) *common.Response { return &expectedResponse })

	msg := common.Message{Code: common.Event, Namespace: namespace, CorrelationID: 1, EventName: event}
	_, response := sendMessage(t, testServerURL(httpServer), msg, validHeaders())
	require.Equal(t, common.Ack, response.Code)
	require.Equal(t, 1, response.CorrelationID)

	var encoded common.Response
	err := json.Unmarshal(response.Payload, &encoded)
	require.NoError(t, err)
	require.Equal(t, expectedResponse, encoded)
}

func TestPingMessageFromClient(t *testing.T) {
	wsServer, httpServer := newServers(t, 0, nil)
	defer httpServer.Close()
	defer wsServer.Close()

	msg := common.Message{Code: common.Ping, Namespace: "nonexistent", CorrelationID: 2}
	_, response := sendMessage(t, testServerURL(httpServer), msg, validHeaders())
	require.Equal(t, common.Pong, response.Code)
	require.Equal(t, 2, response.CorrelationID)
}

func TestOneConnectionAllowedOnly(t *testing.T) {
	wsServer, httpServer := newServers(t, 0, nil)
	defer httpServer.Close()
	defer wsServer.Close()

	_, _, err := websocket.DefaultDialer.Dial(testServerURL(httpServer), validHeaders())
	require.NoError(t, err)

	_, _, err = websocket.DefaultDialer.Dial(testServerURL(httpServer), validHeaders())
	require.Error(t, err)
}

func TestConnectDifferentOriginFail(t *testing.T) {
	origins := []string{"gitlab.com", "localhost"}

	testCases := []struct {
		desc          string
		headers       http.Header
		origins       []string
		expectedError bool
	}{
		{
			desc:          "with empty origin",
			headers:       nil,
			origins:       origins,
			expectedError: true,
		},
		{
			desc:          "with different origin",
			headers:       http.Header{"Origin": []string{"http://example.com"}},
			origins:       origins,
			expectedError: true,
		},
		{
			desc:    "with no valid origins set",
			headers: http.Header{"Origin": []string{"http://example.com"}},
		},
		{
			desc:    "with valid origin",
			headers: http.Header{"Origin": []string{"http://localhost:1234"}},
			origins: origins,
		},
		{
			desc:          "with origin suffixed with a valid origin",
			headers:       http.Header{"Origin": []string{"http://foolocalhost:1234"}},
			origins:       origins,
			expectedError: true,
		},
		{
			desc:          "from a valid origin subdomain",
			headers:       http.Header{"Origin": []string{"http://foo.localhost:1234"}},
			origins:       origins,
			expectedError: true,
		},
		{
			desc:    "with a wildcard as valid origin",
			headers: http.Header{"Origin": []string{"http://foo.localhost:1234"}},
			origins: []string{"foo.bar", "*"},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.desc, func(t *testing.T) {
			wsServer, httpServer := newServers(t, 0, testCase.origins)
			defer httpServer.Close()
			defer wsServer.Close()

			conn, _, err := websocket.DefaultDialer.Dial(testServerURL(httpServer), testCase.headers)

			if conn != nil {
				defer conn.Close()
			}

			if testCase.expectedError == true {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestCloseCallsDisconnectHandlers(t *testing.T) {
	wsServer, httpServer := newServers(t, 0, nil)
	defer httpServer.Close()
	defer wsServer.Close()

	conn, _, err := websocket.DefaultDialer.Dial(testServerURL(httpServer), validHeaders())
	require.NoError(t, err)

	called := false
	err = wsServer.OnDisconnect("foobar", func(*websocket.Conn) *common.Response { called = true; return &common.Response{} })
	require.NoError(t, err)

	cm := websocket.FormatCloseMessage(websocket.CloseNormalClosure, "foobar")
	err = conn.WriteMessage(websocket.CloseMessage, cm)
	require.NoError(t, err)

	before := time.Now()
	for called == false {
		if time.Now().Sub(before).Seconds() > 10 {
			break
		}
	}

	require.True(t, called)
}

func TestWebsocketPingPongDeadline(t *testing.T) {
	duration := 1 * time.Second
	wsServer, httpServer := newServers(t, duration, nil)
	defer httpServer.Close()
	defer wsServer.Close()

	conn, _, err := websocket.DefaultDialer.Dial(testServerURL(httpServer), validHeaders())
	require.NoError(t, err)
	conn.SetPingHandler(func(string) error { return nil })

	time.Sleep(duration + 1*time.Second)
	_, _, err = conn.ReadMessage()
	require.Error(t, err)
	require.Contains(t, err.Error(), "i/o timeout")
}

func newServers(t *testing.T, duration time.Duration, origins []string) (*Server, *httptest.Server) {
	wsServer, err := NewServer(origins, duration)
	require.NoError(t, err)

	s := httptest.NewServer(http.HandlerFunc(wsServer.ServeHTTP))
	return wsServer, s
}

func testServerURL(server *httptest.Server) string {
	return "ws" + strings.TrimPrefix(server.URL, "http")
}

func sendMessage(t *testing.T, url string, msg interface{}, headers http.Header) (*websocket.Conn, common.Message) {
	ws, _, err := websocket.DefaultDialer.Dial(url, headers)
	require.NoError(t, err)

	err = ws.WriteJSON(msg)
	require.NoError(t, err)

	var response common.Message
	err = ws.ReadJSON(&response)
	require.NoError(t, err)

	return ws, response
}

func validHeaders() http.Header {
	return http.Header{"Origin": []string{"http://localhost"}}
}
